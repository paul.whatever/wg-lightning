#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

// Set the data signal pin
#define data 15
// Set the amount of total individual leds
#define amount 600

// Initialize LED Strip with it's properties
Adafruit_NeoPixel strip = Adafruit_NeoPixel(amount, data, NEO_GBRW);

void setup() {
  // Set the data signal pin as ready
  strip.begin();
  // Turn off every single LED
  strip.clear();

  // First beam
  for (size_t i = 18; i < 25; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));
  
  // Second beam
  for (size_t i = 61; i < 68; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Third beam
  for (size_t i = 105; i < 112; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Fourth beam
  for (size_t i = 147; i < 153; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Fifth beam
  for (size_t i = 189; i < 196; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Sixth beam
  for (size_t i = 233; i < 240; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Seventh beam
  for (size_t i = 279; i < 287; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Eigth beam
  for (size_t i = 323; i < 330; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Ninth beam
  for (size_t i = 369; i < 376; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Tenth beam
  for (size_t i = 415; i < 422; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Eleventh beam
  for (size_t i = 461; i < 468; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Twelveth beam
  for (size_t i = 507; i < 514; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Thirteenth beam
  for (size_t i = 552; i < 560; i++)
    strip.setPixelColor(i, strip.Color(0, 0, 0, 255));

  // Transmit pixel data to the strip
  strip.show(); }


void loop() {}